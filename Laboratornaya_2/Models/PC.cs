﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace Laboratornaya_2.Models
{
    public class PC
    {
        private string _file;
        public string File
        {
            get { return _file; }
            private set { }
        }

        public PC(string file,int port)
        {
            _file = file;
            SendMessageFromSocket(port);
        }

        void SendMessageFromSocket(int port)
        {
            IPHostEntry ipHost = Dns.GetHostEntry("localhost");
            IPAddress ipAddr = ipHost.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, port);

            Socket sender = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            
            sender.Connect(ipEndPoint);
            
            string message = File;
            
            byte[] msg = Encoding.UTF8.GetBytes(message);
            
            int bytesSent = sender.Send(msg);
            
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
        }
    }
}

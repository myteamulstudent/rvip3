﻿using System;
using System.Collections.Concurrent;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace Laboratornaya_2.Models
{
    public static class Printer
    {
        private static ConcurrentDictionary<Guid, string> _filesToPrint = new ConcurrentDictionary<Guid, string>();
        private static object _syncObjToReceiveFile = new object();
        private static object _syncObjToPrint = new object();
        private static Random random;
        public static Action<string> PrintFn;

        public async static void OpenSocket(int port)
        {
           
        }

        public async static void TakeFileToPrint(string fileName)
        {
            await Task.Run(() =>
            {
                int operationDuration;
                Guid guid;
                lock (_syncObjToReceiveFile)
                {
                    if (random == null)
                        random = new Random();
                    operationDuration = random.Next(500, 5000);

                    PrintFn?.Invoke(string.Format("Начата операция принятия в печать файла {0}, длительность - {1}" + Environment.NewLine,
                        fileName, operationDuration));
                    Thread.Sleep(operationDuration);
                    guid = Guid.NewGuid();
                    _filesToPrint.TryAdd(guid, fileName);
                    PrintFn?.Invoke(string.Format("Закончена операция принятия в печать файла {0}, длительность - {1}" + Environment.NewLine,
                        fileName, operationDuration));
                }

                var fileToOutput = string.Empty;

                lock (_syncObjToPrint)
                {
                    if (_filesToPrint.TryRemove(guid, out fileToOutput))
                    {
                        Thread.Sleep(1000);
                        PrintFn?.Invoke(string.Format("Файл напечатан - {0}" + Environment.NewLine, fileToOutput));
                    }
                    else
                    {
                        PrintFn?.Invoke("Какая-то ошибка. Ну это ж принтер.");
                    }
                }
            });
        }

        private static void PrintToOutput(string text)
        {
            PrintFn?.Invoke(text);
        }

        private static void PrintFile(string file)
        {
            PrintFn?.Invoke(string.Format("Напечатал файл - {0}", file));
        }
    }
}
